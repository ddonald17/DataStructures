#include <iostream>
using namespace std;

struct node {
  int data;
  struct node *next;
};
typedef struct node *nodeptr;

nodeptr getNode(){
  return (nodeptr)malloc(sizeof(struct node));
}
nodeptr head = NULL;

//insert at the begining of the linked list
void insert (int data){
  nodeptr newNode = getNode();
  newNode->data = data;
  newNode->next = head;
  head = newNode;
}

//display a linked list
void display()
{
  nodeptr temp=head;
  while(temp!=NULL){
    cout<<temp->data<<" -> ";
    temp=temp->next;
  }
  cout<<"NULL"<<endl;
}

//to detect a cycle
bool detectCycle(){
  nodeptr fast = head;
  nodeptr slow = head;

  while(fast!=NULL && fast->next!=NULL){
    slow = slow->next;
    fast = fast->next->next;

    if(fast == slow)
      return true;    
  }
  return false;
}


// fuction to make a cycle from last node to a node based on position
void makeCycle(int pos){
  nodeptr temp = head;
  nodeptr startNode;

  int count = 1;

  while(temp->next != NULL){
    if(count == pos){
      startNode = temp;
    }
    temp = temp->next;
    count++;
  }
  temp->next = startNode;
}

//remove the cycle
void removeCycle(){
  nodeptr slow = head;
  nodeptr fast = head;

  do{
    slow = slow->next;
    fast = fast->next->next;
  }while(slow != fast);

  fast = head;
  while (slow->next != fast->next){
    slow = slow->next;
    fast = fast->next;
  }

  slow->next = NULL;
}
int main() {
  insert(20);
  insert(30);
  insert(40);
  insert(10);
  insert(2);
  insert(3);
  display();

  makeCycle(3);
  cout<<detectCycle()<<endl; //displays 1 if cycle is present

  removeCycle();
  cout<<detectCycle()<<endl;
  display();
  
  return 0;
}
