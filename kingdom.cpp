#include<iostream>
#include<vector>
#include<algorithm>

using namespace std;

int kingdom(vector <int> &A){
  int days = 0;
  int x = 0;
  int initialTroops = 1;

  while(!A.empty()){
    if(x == 0){
      x = initialTroops;
    }
    else{
      x = x + initialTroops;
    }
    
    if(find(A.begin(),A.end(),x) != A.end()){
      A.erase(find(A.begin(),A.end(),x));
      x = 0;
      initialTroops++;
    }
    days++;
  }
  return days;
}
int main() {
  vector <int> v;
  vector <int>::iterator it;
  v = {4,3,1};
    // it = find(v.begin(),v.end(),3);
    // cout<<(*it);
  int x = kingdom(v);
  cout<<x;
}
