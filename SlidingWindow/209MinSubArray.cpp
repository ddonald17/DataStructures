//LeetCode 209
//209. Minimum Size Subarray Sum
// Input: target = 7, nums = [2,3,1,2,4,3]
// Output: 2
// Explanation: The subarray [4,3] has the minimal length under the problem constraint.

#include<iostream>
using namespace std;


int minSubArraySum(int arr[], int n ,int target ){
    int ans = n+1;
    int sum = 0;
    int left= 0 , right = 0;

    for(right = 0;right<n;right++){
        sum = sum + arr[right];

        while(sum >= target){
            ans = min( right-left+1 , ans); //right-left+1 is the window size
            sum -=arr[left++];
        }
    }
    return ans;
}

int main(){

    int arr[] = {2,3,1,2,4,3} , target = 7 , n =6;
    int res = minSubArraySum(arr,n,target);

    if(res == n+1){
        cout<<"subarray doesn't exists";
    } 
    else{
        cout<<"minimum subarray = "<<res;
    }
    return 0;
}