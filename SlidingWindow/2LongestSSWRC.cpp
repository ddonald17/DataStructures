// 3. Longest Substring Without Repeating Characters
// Input: s = "abcabcbb"
// Output: 3
// Explanation: The answer is "abc", with the length of 3.

#include<iostream>
#include<set>

using namespace std;

class Solution {
public:
    int lengthOfLongestSubstring(string s) {
        set <char> Set;
        int left=0;
        int right=0;
        int sum = 0;

        while(right<s.size()){
            auto it = Set.find(s[right]);

            if(it == Set.end()){
                sum = max(sum,right-left+1);
                Set.insert(s[right]);
                right++;
            }
            else{
                Set.erase(s[left]);
                left++;
            }
        }
        return sum;

    }
};

int main(){
    Solution A;
    string s;
    
    cout<<"enter a string ";
    cin>>s;
    cout<<"Longest substring length without repeating characters ="<<A.lengthOfLongestSubstring(s);
    
    return 0;
}