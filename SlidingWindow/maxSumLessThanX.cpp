//max sum subarray of size=k and sum < x

#include<iostream>
using namespace std;

void maxSumSubArray(int arr[],int n,int k,int x){
    int sum = 0, ans = 0, i;

    //to find sum of first k elements 
    for(i=0;i<k;i++){
        sum += arr[i];
    }

    //set the answer to sum if it's less than x
    if(sum < x)
      ans = sum;
    
    for(i=k;i<n;i++){
        sum = sum - arr[i-k] + arr[i];

        if(sum<x){
            ans = max(ans , sum);
        }
    }
    cout<<ans<<" is the sum of subarray of length "<<k<<" less than "<<x;
}

int main(){

    int a[] = {5,3,2,6,5,12};
    int n = 6;
    int k=3;
    int x = 20;
    maxSumSubArray(a,n,k,x);
    return 0;
}