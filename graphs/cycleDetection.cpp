// codestudio cycle detection problem

#include<unordered_map>
#include<queue>
#include<set>
void prepareAdjList(unordered_map<int,set<int>> &adj,vector<vector<int>>& edges)
{
    for(int i=0;i<edges.size();i++){
        int u = edges[i][0];
        int v = edges[i][1];
        
        adj[u].insert(v);
        adj[v].insert(u);
    }
}

bool isCyclic(int src,unordered_map<int,bool> &visited,unordered_map<int,set<int>> &adj){
    unordered_map<int,int> parent;
    queue <int> q;
    
    parent[src] = -1;
    visited[src] = 1;
    q.push(src);
    
    while(!q.empty()){
        int front = q.front();
        q.pop();
        
        for(auto neighbour:adj[front]){
            if(visited[neighbour] && neighbour!=parent[front]){
                return true;
            }
            
            else if(!visited[neighbour]){
                q.push(neighbour);
                visited[neighbour] = 1;
                parent[neighbour] = front;
            }
        }
    }
    return false;
}
string cycleDetection (vector<vector<int>>& edges, int n, int m)
{
    unordered_map<int,set<int>> adj;
    //preparing  adjecency list 
    prepareAdjList(adj,edges);
    
    unordered_map<int,bool> visited;
    
    // to handle disconnected graphs
    for(int i=0;i<n;i++){
        if(!visited[i]){
            bool ans = isCyclic(i,visited,adj);
            
            if(ans)
                return "Yes";
        }
    }
    return "No";
}
