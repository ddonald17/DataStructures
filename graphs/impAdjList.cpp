#include<iostream>
#include<unordered_map>
#include<list>
using namespace std;
template <typename t>

class graph{

public:
  unordered_map<t,list<int>> adj;

  void addEdge(int u,int v, bool direction){
    // direction = 0 -> undirected
    //direction = 1 -> directed graphs

    adj[u].push_back(v);

    if(direction == 0)
      adj[v].push_back(u);
    
  }

  void printAdjList(){

    for(auto i:adj){

      cout<<i.first<<"->";

      for(auto j:i.second){
        cout<<j<<" ";
      }
      cout<<endl;
    }
  }


};

int main(){

  graph<int> g;
  int n,m;
  cout<<"enter the number of nodes ";
  cin>>n;

  cout<<"enter the number if edges ";
  cin>>m;

  cout<<"enter the nodes and  corresponding egdes"<<endl;
  for(int i=0;i<m;i++){
    int u,v;
    cin>>u>>v;
    g.addEdge(u,v,0);
  }

  g.printAdjList();
return 0;
}