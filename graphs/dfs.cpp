//codeStudio DFS Problem solution

#include<unordered_map>
#include<set>


void prepareAdjList(unordered_map<int,set<int>> &adj, vector<vector<int>> &edges){
    for(int i=0;i<edges.size();i++){
      int u = edges[i][0];
      int v = edges[i][1];
        
      adj[u].insert(v);
      adj[v].insert(u);
        
    }
}

void dfs(int node,unordered_map<int,set<int>> &adj,unordered_map<int,bool> &visited,vector<int> &comp){
    comp.push_back(node);
    visited[node] = 1;
    
    for(auto i: adj[node]){
        if(!visited[i])
            dfs(i,adj,visited,comp);
    }
}

vector<vector<int>> depthFirstSearch(int V, int E, vector<vector<int>> &edges)
{
    unordered_map<int,set<int>> adj;
    prepareAdjList(adj,edges);
    
    vector<vector<int>> ans;
    unordered_map<int,bool> visited;
    
    for(int i=0;i<V;i++){
        
        if(!visited[i]){
            vector<int> comp;
            dfs(i,adj,visited,comp);
            ans.push_back(comp);
        }
    }
    
    return ans;
}