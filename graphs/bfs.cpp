//coding ninjas bfs solution

#include<unordered_map>
#include<set>
#include<queue>
void prepareAdjList(unordered_map<int,set<int>> &adj,vector<pair<int,int>> &edges){
    
    for(int i = 0;i<edges.size();i++){
       int u = edges[i].first;
       int v = edges[i].second;
        
        adj[u].insert(v);
        adj[v].insert(u);
        
    }
}

void bfs(unordered_map<int,set<int>> &adj,unordered_map<int,bool> &visited,vector<int> &ans,int node){
    
    queue<int> q;
    q.push(node);
    visited[node] = 1;
    
    while(!q.empty()){
        int x = q.front();
        q.pop();
        
        ans.push_back(x);
        
        for(auto i:adj[x]){
            if(!visited[i])
            {
            q.push(i);
            visited[i] = 1;
            }
        }
    }
}

vector<int> BFS(int vertex, vector<pair<int, int>> edges)
{
    // Write your code here
    unordered_map<int,set<int>> adj;
    prepareAdjList(adj,edges);
    
    vector<int> ans;
    unordered_map<int,bool> visited;
    
    
    //performing bfs
    for(int i = 0; i<vertex ; i++){
        if(!visited[i]){
            bfs(adj,visited,ans,i);
        }
    }
    return ans;
}