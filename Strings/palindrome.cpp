#include<iostream>

using namespace std;

bool isValidChar(char a){
    if(isalpha(a)||isdigit(a)){
        return true;
    }
    return false;
}

bool isPalindrome(string s){
    int left=0;
    int right = s.length() - 1;

    while (left < right){
        if(!isValidChar(s[left]))
            left++;
        else if(!isValidChar(s[right]))
            right--;

        else if(tolower(s[left])!=tolower(s[right])){
            return false;
        }
        else{
            right--;
            left++;
        }
    }
    return true;

}




int main(){

    string s = "....Iam5445maI,...";
    
    if(isPalindrome(s))
        cout<<s<<" is a palindrome";
    else 
        cout<<s<<" is not a palindrome";
    return 0;
}