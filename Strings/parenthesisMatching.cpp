#include<iostream>
#include<stack>

using namespace std;

bool isValidParanthesis(string str){
    stack <char> s;
    char ch;

    for(int i=0; i<str.length();i++){

        if(str[i]=='('||str[i]=='{'||str[i]=='['){
            s.push(str[i]);
            continue;
        }
        if(s.empty())
            return false;
        switch(str[i]){
            case ')':
                ch = s.top();
                s.pop();
                if(ch=='{'||ch=='[')
                    return false;
                break;
            case '}':
                ch = s.top();
                s.pop();
                if(ch=='('||ch=='[')
                    return false;
                break;
            case ']':
                ch = s.top();
                s.pop();
                if(ch=='{'||ch=='(')
                    return false;
                break;
        }

    }
    return (s.empty());
}

int main(){
    string s = "[]";

    if(isValidParanthesis(s))
        cout<<"parathesis matching";
    else
        cout<<"not matching";
    return 0;
}