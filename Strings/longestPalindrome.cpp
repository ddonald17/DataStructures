// 5. Longest Palindromic Substring
// Given a string s, return the longest palindromic substring in s.
//  Input: s = "babad"
// Output: "bab"
// Explanation: "aba" is also a valid answer.

#include<iostream>


using namespace std;

int getIndexOfString(int left,int right){

}

string longestPalindrome(string s) {
    string result;
    int resultLength = 0;

    for(int i=0;i<s.length();i++){
        int left=i;
        int right =i;

        while(left>0 && right<s.length() && s[left]==s[right]){
            if(right-left+1 > resultLength){
                result = s.substr(left,right-left+1);
                resultLength = right-left+1;
            }
            left--;
            right++;
        }
    }
     for(int i=0;i<s.length();i++){
        int left=i;
        int right =i+1;

        while(left>0 && right<s.length() && s[left]==s[right]){
            if(right-left+1 > resultLength){
                result = s.substr(left,right-left+1);
                resultLength = right-left+1;
            }
            left--;
            right++;
        }
    }
    return result; 
}

int main(){


    string s = "bbabcbcab";
    cout<<longestPalindrome(s);
    return 0;
}