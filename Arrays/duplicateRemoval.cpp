#include<iostream>
#include<vector>
#include<set>
#include<algorithm>

using namespace std;
vector <int> v;
vector <int>::iterator it;

void printWithoutDup(){
    for(auto i=v.begin();i<v.end();i++){
         it = find(v.begin(),v.end(),*i);

         if((i-v.begin()) == it-v.begin())
            cout<<*i<<" ";
    }
}

void setImplementation(){
    set <int> s;

    for(auto i=v.begin();i!=v.end();i++){
        s.insert(*i);
    }
    for(auto i:s){
        cout<<i<<" ";
    }cout<<endl;
}


int main(){
   
    int n;
    cout<<"enter number of elements"<<endl;
    cin>>n;

    cout<<"enter array element"<<endl;
    for(int i = 0 ; i< n ; i++) {
     int element;
     cin >> element; 
     v.push_back(element);
}

    printWithoutDup();
    setImplementation();
    return 0;
}