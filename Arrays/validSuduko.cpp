#include <bits/stdc++.h>
using namespace std;

class Solution {
public:
    bool isValidSudoku(vector<vector<char>>& board) {
        vector<unordered_set<char>> squares(9);

        for(int i=0;i<board.size();i++){
            unordered_set<char> row;
            unordered_set<char> col;
            for(int j=0;j<board.size();j++){
                //checking for rows
                if(board[i][j]!='.'){
                    if(!row.insert(board[i][j]).second){
                        return false;
                    }

                    //checking for squares
                    int s = ((i / 3) * 3) + (j / 3);
                    if(!squares[s].insert(board[i][j]).second){
                        return false;
                    }
                }

                if(board[j][i] != '.'){
                    //checking for colomns
                    if(!col.insert(board[j][i]).second){
                        return false;
                    }
                }
            }
        }

        return true;
        
    }
};
